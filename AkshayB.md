# LOAD BALANCING

## What is load balancing and the need for load balancing?

Load balancing is the process, or set of processes **routing** or **evenly distributing** traffic (a.k.a requests) over a set of **Servers**.
This, in turn, reduces the uneven distribution of tasks over the servers thereby making the overall computational process **more efficient**.

There are several algorithms and techniques to achieve load balancing as we shall see below. All these techniques depend upon the following parameters that have to be taken into consideration:

1. **Size of tasks** : The execution time of the tasks are difficult to know beforehand to distribute to the respective servers. One of the approaches to this problem is to make use of the task's metadata. Depending on the previous execution time of similar tasks, we can decide the load-balancing technique to be implemented.  

2. **Dependencies** : Check if the tasks depend on each other and make use of their execution time and distribute tasks using algorithms like [job scheduler](https://en.wikipedia.org/wiki/Job_scheduler).

3. **Breaking down** complex tasks into sub-tasks can reduce the complexity.




## Types of Algorithms

* **Static Algorithms** : These do not consider the state of the machine for distributing the requests. They only consider properties like server resources, the number of processors, their power, and speed. These algorithms make use of centralized routers called *Masters* which distribute the tasks.

* **Dynamic Algorithms** : These are more general-purpose and also efficient for complex handling but they require an exchange of information between the nodes and this may result in loss of efficiency. They take into account the current load of each node.


Load balancing techniques can also be selected based on the IP range of the client (especially for location-based applications) and also on the data provided in the request headers. The algorithms are suited for different situations and compromise must be found to select the best technique based on application-specific requirements.



## Some commonly used Load Balancing Algorithms

* **Round Robin** : This is a simple distribution technique which sequentially distributes the load, one server after another. It is easy to understand and implement. However, if the servers are of uneven hardware specifications, for example, *server-1* is more powerful than *server-2*, the Round Robin method is not efficient as it sends the same number of requests to both the servers which cause uneven distribution. Round Robin does not make use of the current state of the servers.

* **Weighted Round Robin** : A solution to the problem arising from the above situation is to distribute the tasks based on the capacity of each server. When the Load Balancing is set-up, the hardware capabilities of each server is determined and given a certain weightage value. The tasks are distributed based on these weightage values. So, a server having more capacity is given more requests to handle. This reduces the problem of overloading the servers.

* **Least Connections** : This technique makes use of the active state of the server. It sends requests to servers based on which server is least used at the moment, that is, based on the number of active connections of each server, and allocates the requests accordingly. If a server which has more capacity but fewer active connections than the next server whose connections are still active, the request is sent to the former server. This takes up more time for computing.

* **Weighted Least Connections** : This also takes into consideration the current active connections and in addition to that, also considers the respective weightage/capacity of the servers and handles distribution accordingly.

* **IP Hashing** : The requests from the clients are distributed over the servers based on the Client's IP and only those servers which are handling the specific IP ranges are given these tasks.

* **Random** : As the name suggests, these match the client requests and servers in random, using a random number generator function. Care should be taken to avoid overloading of certain servers while other servers are idle.


### Note on Sticky Sessions (Session Persistence)

If a request is having lots of information, for example, a Flipkart server handling the *cart* details of a client, the details are present in the Client-side as cache memory. In this state, if the load balancing algorithms redirect the request every time to different servers, the result will be loss of data. Therefore, the employed algorithm should make the load balancer to send these requests to the same server and handle instances of the requests.



## Vertical-Scaling v/s Horizontal Scaling

**Vertical Scaling** or *scaling-up* is adding more computational resources to the existing server as the demand arises. This usually is adding more power to the system. These are done when the amount of data/requests to be handled goes up. However, it is difficult to foresee the demand of the servers in long-term applications and hence Horizontal Scaling is preferred.

**Horizontal Scaling** or *scaling out* is the process of adding more machines to the ecosystem and thereby adding more resources to handle the requests. In simpler words, this means adding more servers/nodes to handle the tasks and thereby reducing the dependence on a single cluster. The load is decreased due to the distribution between separate server nodes.


The difference between the two lies in how computational resources are added. Horizontal-scaling involves partitioning the data in which each node contains only part of the data. However, in Vertical-scaling, each node has the entire data. Horizontal-scaling is more feasible and advantageous due to the fact that it is easier to add more machines into the ecosystem as and when required than in the case of Vertical-scaling which involves upgrading the existing systems which cannot be determined before-hand for long term projects. Horizontal scaling also allows us to have the option of not having to take down the servers when upgrading.





### **REFERENCES**

* [What is Load Balancing](https://www.youtube.com/watch?v=K0Ta65OqQkY)
* [Session Persistence](https://www.youtube.com/watch?v=7LMaAVwZE2c)
* [Load Balancing](https://en.wikipedia.org/wiki/Load_balancing_(computing)#)
* [How Load Balancing Works](https://www.youtube.com/watch?v=escR-07yVAs)
* [Vertical and Horizontal Scaling 1](https://www.esds.co.in/blog/vertical-scaling-horizontal-scaling/#sthash.Ee1M8q0b.dpbs)
* [Vertical and Horizontal Scaling 2](https://www.missioncloud.com/blog/horizontal-vs-vertical-scaling-which-is-right-for-your-app#:~:text=With%20vertical%20scaling%20(a.k.a.%20%E2%80%9Cscaling,memory%20workload%20across%20multiple%20devices.))
* [Horizontal Scaling for Databases](https://medium.com/@abhinavkorpal/scaling-horizontally-and-vertically-for-databases-a2aef778610c)